;;; -*- Mode: lisp; Syntax: common-lisp; Package: :hemd -*-

(in-package :hemd)

;;  Global function executed when certain demo items are selected.
(defun identify-item (top-level-obj string)
  (format t "Object ~S now has value ~S.~%" top-level-obj string))


;;;  These functions are included to show that selection of one of the
;;;  menu items causes the associated local function to be called.
(defun my-cut (gadget item-string)
  (declare (ignore gadget item-string))
  (format t "Function CUT called~%"))

(defun my-copy (gadget item-string)
  (declare (ignore gadget item-string))
  (format t "Function COPY called~%"))

(defun my-paste (gadget item-string)
  (declare (ignore gadget item-string))
  (format t "Function PASTE called~%"))

(defun my-undo (gadget item-string)
  (declare (ignore gadget item-string))
  (format t "Function UNDO called~%"))

(defun my-cancel (gadget item-string)
  (declare (ignore gadget item-string))
  (format t "Function CANCEL called~%"))


(defvar *current-app* nil)

(defun do-go (&key notification-thunk dont-enter-main-event-loop (double-buffered-p t))

  (setf *current-app* (create-instance 'hemd-win inter:interactor-window
			(:title "Music Module")
			(:background-color
			 (create-instance nil opal:color
                         (:red 0.4)
                         (:green 0.6)
                         (:blue 0.2)))
			(:double-buffered-p double-buffered-p)
			(:left 10) (:top 50) (:width 600) (:height 600)))

  (s-value hemd-win
	   :aggregate
	   (create-instance 'hemd-top-agg opal:aggregate))

  ;; If we get clobbered by the window manager, let the demos
  ;; controller know (if it's there).  Protocol, calling application
  ;; is allowed to specify a callback function to communicate the
  ;; window has been closed.
  (when (and notification-thunk *current-app*)
    (pushnew
     #'(lambda (win)
	 (declare (ignore win))
	 (funcall notification-thunk *current-app*))
     (g-value hemd-win :destroy-hooks)))

  (create-instance 'hemd-text-buttons-obj garnet-gadgets:text-button-panel
    (:constant T)
    (:left 10) (:top 10)
    (:items '("Chopin" "Mozart" "Beethoven" "Bach" "Strauss"))
    (:selection-function #'identify-item))

  (opal:add-components HEMD-TOP-AGG hemd-text-buttons-obj)
  (opal:update hemd-win)

  (create-instance 'hemd-x-buttons-obj garnet-gadgets:x-button-panel
    (:constant T)
    (:left 400) (:top 300)
    (:items '("scr-trill-p" "page-trill-p" "indicator-text-p"
	      "int-feedback-p")))
  (opal:add-components HEMD-TOP-AGG hemd-x-buttons-obj)


  (create-instance 'hemd-h-scroll-obj garnet-gadgets:h-scroll-bar
    (:constant T :except :scr-trill-p :page-trill-p
  	       :indicator-text-p :int-feedback-p)
    (:left 10) (:top 350)
    (:scr-trill-p
     (o-formula (member "scr-trill-p" (gv hemd-x-buttons-obj :value)
  			:test #'equal)))
    (:page-trill-p
     (o-formula (member "page-trill-p" (gv hemd-x-buttons-obj :value)
  			:test #'equal)))
    (:indicator-text-p
     (o-formula (member "indicator-text-p" (gv hemd-x-buttons-obj :value)
  			:test #'equal)))
    (:int-feedback-p
     (o-formula (member "int-feedback-p" (gv hemd-x-buttons-obj :value)
  			:test #'equal))))
 (opal:add-components HEMD-TOP-AGG hemd-h-scroll-obj)
 (opal:update hemd-win)


  (create-instance 'DEMO-GAUGE-OBJ garnet-gadgets:gauge
    (:constant T)
    (:left 30) (:top 430)
    (:title "Temperature")
    (:int-feedback-p NIL)
    )
  (opal:add-components HEMD-TOP-AGG DEMO-GAUGE-OBJ)
  (opal:update hemd-win)


  (create-instance 'DEMO-GAUGE2-OBJ garnet-gadgets:gauge
    (:constant T)
    (:left 300) (:top 455)
    (:val-1 45) (:val-2 90)
    (:width 200) (:num-marks 12)
    (:title "Pressure")
    (:polygon-needle-p NIL))
  (opal:add-components HEMD-TOP-AGG DEMO-GAUGE2-OBJ)
  (opal:update hemd-win)


  (create-instance 'DEMO-V-SLIDER-OBJ garnet-gadgets:v-slider
    (:constant T)
    (:left 175) (:top 10)
    (:val-1 0) (:val-2 50)
    (:num-marks 6))
  (opal:add-components HEMD-TOP-AGG DEMO-V-SLIDER-OBJ)
  (opal:update hemd-win)



  (create-instance 'DEMO-RADIO-BUTTONS-OBJ garnet-gadgets:radio-button-panel
    (:constant T)
    (:left 420) (:top 60)
    (:items '(:left :center :right)))
  (g-value DEMO-RADIO-BUTTONS-OBJ :value)
  (s-value DEMO-RADIO-BUTTONS-OBJ :value :left)
  (opal:add-components HEMD-TOP-AGG DEMO-RADIO-BUTTONS-OBJ)
  (opal:update hemd-win)


  (create-instance 'DEMO-LAB-OBJ garnet-gadgets:labeled-box
    (:constant T)
    (:left 280) (:top 10)
    (:label-string "Title:")
    (:value "Menu"))
  (opal:add-components HEMD-TOP-AGG DEMO-LAB-OBJ)
  (opal:update hemd-win)


  (create-instance 'DEMO-MENU-OBJ garnet-gadgets:menu
    (:constant T :except :h-align :title)
    (:left 300) (:top 50)
    (:selection-function #'identify-item)
    (:h-align (o-formula (gv demo-radio-buttons-obj :value)))
    (:title (o-formula (gv demo-lab-obj :value)))
    (:items '(("Cut" my-cut) ("Copy" my-copy)
  	      ("Paste" my-paste) ("Undo" my-undo))))
  (opal:add-components HEMD-TOP-AGG DEMO-MENU-OBJ)
  (opal:update hemd-win)


  (create-instance 'DEMO-TRILL-OBJ garnet-gadgets:trill-device
    (:constant T)
    (:left 300) (:top 225)
    (:val-2 NIL))
  (opal:add-components HEMD-TOP-AGG DEMO-TRILL-OBJ)

  (create-instance 'DEMO-SCROLLING-STRING-OBJ garnet-gadgets:scrolling-labeled-box
    (:constant T)
    (:left 10) (:top 300)
    (:width 250)(:label-string "Scrolling String:")
    (:value "Long strings can be input and scrolled horizontally"))

  (opal:add-components HEMD-TOP-AGG DEMO-SCROLLING-STRING-OBJ)

  (opal:update hemd-win)

  (unless dont-enter-main-event-loop
    (inter:main-event-loop))

  )

(defun Do-Stop ()
  (opal:destroy hemd-win))
